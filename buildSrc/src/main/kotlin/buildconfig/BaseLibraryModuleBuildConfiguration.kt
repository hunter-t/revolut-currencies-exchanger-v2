package buildconfig

import com.android.build.gradle.BaseExtension
import com.android.build.gradle.internal.dsl.BaseFlavor
import com.android.build.gradle.internal.dsl.BuildType
import com.android.build.gradle.internal.dsl.JavaCompileOptions
import com.android.build.gradle.internal.dsl.SigningConfig
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project

/**
 * General Android Library Module configuration.
 *
 * Created by Ruslan Arslanov on 15/02/2020.
 */
open class BaseLibraryModuleBuildConfiguration(
    protected val project: Project,
    protected val androidExtension: BaseExtension
) : BaseBuildConfiguration(
    project,
    androidExtension
) {

    override val versionCode: Int? = null
    override val versionName: String? = null
    override val applicationId: String? = null
    override val manifestPlaceholders: Map<String, Any>? = null

    override fun configureSigningConfigs(
        signingConfigs: NamedDomainObjectContainer<SigningConfig>
    ) = Unit

    override fun setupBuildConfigFields(defaultProductFlavor: BaseFlavor) = Unit

    override fun configureJavaCompileOptions(javaCompileOptions: JavaCompileOptions) = Unit

    override fun configureBuildTypes(
        buildTypes: NamedDomainObjectContainer<BuildType>
    ) {
        buildTypes.apply {
            getByName("debug").apply {
                isDebuggable = true
            }
            getByName("release").apply {
                isDebuggable = false
            }
        }
    }

}
