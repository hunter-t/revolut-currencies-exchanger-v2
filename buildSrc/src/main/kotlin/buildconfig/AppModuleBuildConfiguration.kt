package buildconfig

import com.android.build.gradle.BaseExtension
import com.android.build.gradle.internal.dsl.BaseFlavor
import constants.ApplicationConstants
import org.gradle.api.Project
import util.stringConfigField
import java.util.*

/**
 * Config for the main app module.
 *
 * Created by Ruslan Arslanov on 15/02/2020.
 */
class AppModuleBuildConfiguration(
    project: Project,
    androidExtension: BaseExtension
) : BaseLibraryModuleBuildConfiguration(
    project = project,
    androidExtension = androidExtension
) {

    companion object {
        const val BUILD_CONFIG_FIELD_API_BASE_URL = "API_BASE_URL"
    }

    override val applicationId: String? = ApplicationConstants.APP_ID
    override val versionCode: Int? = ApplicationConstants.APP_VERSION_CODE
    override val versionName: String? = ApplicationConstants.APP_VERSION_NAME

    override fun setupBuildConfigFields(defaultProductFlavor: BaseFlavor) {
        super.setupBuildConfigFields(defaultProductFlavor)
        val encodedBaseUrl = Base64
            .getUrlEncoder()
            .encodeToString(ApplicationConstants.API_BASE_URL.toByteArray(Charsets.UTF_8))
        defaultProductFlavor.stringConfigField(BUILD_CONFIG_FIELD_API_BASE_URL, encodedBaseUrl)
    }

}