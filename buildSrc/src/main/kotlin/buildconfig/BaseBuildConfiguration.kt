package buildconfig

import com.android.build.gradle.BaseExtension
import com.android.build.gradle.internal.dsl.BaseFlavor
import com.android.build.gradle.internal.dsl.BuildType
import com.android.build.gradle.internal.dsl.JavaCompileOptions
import com.android.build.gradle.internal.dsl.SigningConfig
import constants.SdkVersions
import org.gradle.api.JavaVersion
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project

/**
 * Created by Ruslan Arslanov on 15/02/2020.
 */
abstract class BaseBuildConfiguration(
    private val project: Project,
    private val androidExtension: BaseExtension
) {

    abstract val manifestPlaceholders: Map<String, Any>?

    abstract val versionCode: Int?
    abstract val versionName: String?
    abstract val applicationId: String?

    fun configure() {
        with(androidExtension) {
            compileSdkVersion(SdkVersions.COMPILE_SDK_VERSION)
            defaultConfig {
                minSdkVersion = SdkVersions.MIN_SDK_VERSION
                targetSdkVersion = SdkVersions.TARGET_SDK_VERSION
                setupMainArguments(this)
                setupBuildConfigFields(this)
                configureJavaCompileOptions(javaCompileOptions)
                manifestPlaceholders?.let(::addManifestPlaceholders)
            }
            sourceSets {
                getByName("main").java.srcDirs("src/main/kotlin")
            }
            compileOptions {
                sourceCompatibility = JavaVersion.VERSION_1_8
                targetCompatibility = JavaVersion.VERSION_1_8
            }
            configureSigningConfigs(signingConfigs)
            configureBuildTypes(buildTypes)
        }
    }

    private fun setupMainArguments(defaultProductFlavor: BaseFlavor) {
        if (applicationId != null) {
            defaultProductFlavor.applicationId = applicationId
        }
        if (versionCode != null) {
            defaultProductFlavor.versionCode = versionCode
        }
        if (versionName != null) {
            defaultProductFlavor.versionName = versionName
        }
    }

    abstract fun setupBuildConfigFields(defaultProductFlavor: BaseFlavor)

    abstract fun configureJavaCompileOptions(javaCompileOptions: JavaCompileOptions)

    abstract fun configureBuildTypes(buildTypes: NamedDomainObjectContainer<BuildType>)

    abstract fun configureSigningConfigs(signingConfigs: NamedDomainObjectContainer<SigningConfig>)

}