/**
 * Created by Ruslan Arslanov on 15/02/2020.
 */

object Kotlin {
    const val STDLIB = "org.jetbrains.kotlin:kotlin-stdlib:${GradlePluginsVersions.KOTLIN_PLUGIN}"
}

object AndroidX {
    private const val X_VERSION = "1.1.0"
    private const val LEGACY_VERSION = "1.0.0"

    const val CORE = "androidx.core:core-ktx:$X_VERSION"
    const val APPCOMPAT = "androidx.appcompat:appcompat:$X_VERSION"
    const val FRAGMENT = "androidx.fragment:fragment-ktx:$X_VERSION"
    const val ANNOTATIONS = "androidx.annotation:annotation:$X_VERSION"

    const val CORE_UI = "androidx.legacy:legacy-support-core-ui:$LEGACY_VERSION"
    const val CORE_UTILS = "androidx.legacy:legacy-support-core-utils:$LEGACY_VERSION"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:1.1.3"
}

object MaterialComponents {
    const val LIBRARY = "com.google.android.material:material:1.2.0-alpha03"
}

object Dagger2 {
    private const val VERSION = "2.21"
    const val ANNOTATIONS = "javax.annotation:jsr250-api:1.0"
    const val LIBRARY = "com.google.dagger:dagger:$VERSION"
    const val COMPILER = "com.google.dagger:dagger-compiler:$VERSION"
    const val ANDROID_EXTENSIONS = "com.google.dagger:dagger-android:$VERSION"
    const val SUPPORT_EXTENSIONS = "com.google.dagger:dagger-android-support:$VERSION"
    const val ANDROID_EXTENSIONS_COMPILER = "com.google.dagger:dagger-android-processor:$VERSION"
}

object Retrofit {
    private const val VERSION = "2.5.0"
    const val LIBRARY = "com.squareup.retrofit2:retrofit:$VERSION"
    const val RX_ADAPTER = "com.squareup.retrofit2:adapter-rxjava2:$VERSION"
    const val GSON_CONVERTER = "com.squareup.retrofit2:converter-gson:$VERSION"
}

object LifeCycle {
    const val VIEW_MODEL_LIBRARY = "androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0"
    const val VIEW_MODEL_COMPILER = "androidx.lifecycle:lifecycle-compiler:2.2.0"
    const val LIFECYCLE_EXTENSIONS = "android.arch.lifecycle:extensions:2.1.0"
}

object Glide {
    private const val VERSION = "4.8.0"
    const val LIBRARY = "com.github.bumptech.glide:glide:$VERSION"
    const val COMPILER = "com.github.bumptech.glide:compiler:$VERSION"
}

object Navigation {
    private const val NAVIGATION_VERSION = "2.2.1"
    const val UI = "androidx.navigation:navigation-ui-ktx:$NAVIGATION_VERSION"
    const val FRAGMENT = "androidx.navigation:navigation-fragment-ktx:$NAVIGATION_VERSION"
}

object Rx {
    const val RX_JAVA = "io.reactivex.rxjava2:rxjava:2.2.6"
    const val RX_KOTLIN = "io.reactivex.rxjava2:rxkotlin:2.3.0"
    const val RX_ANDROID = "io.reactivex.rxjava2:rxandroid:2.1.0"
    const val RX_RELAY = "com.jakewharton.rxrelay2:rxrelay:2.1.0"
    const val RX_PREFERENCES = "com.f2prateek.rx.preferences2:rx-preferences:2.0.0"
}

object RxBinding {
    private const val VERSION = "3.0.0-alpha2"
    const val CORE = "com.jakewharton.rxbinding3:rxbinding-core:$VERSION"
    const val DESIGN = "com.jakewharton.rxbinding3:rxbinding-material:$VERSION"
    const val APP_COMPAT = "com.jakewharton.rxbinding3:rxbinding-appcompat:$VERSION"
    const val RECYCLER = "com.jakewharton.rxbinding3:rxbinding-recyclerview:$VERSION"
    const val METERIAL_COMPONENTS = "com.jakewharton.rxbinding3:rxbinding-material:$VERSION"
}

object Tools {
    const val GSON = "com.google.code.gson:gson:2.8.5"
    const val TIMBER = "com.github.ajalt:timberkt:1.5.1"
    const val OKHTTP_LOGGER = "com.github.ihsanbal:LoggingInterceptor:3.0.0"
    const val ADAPTER_DELEGATES = "com.hannesdorfmann:adapterdelegates4:4.0.0"
    const val BINARY_PREFERENCES = "com.github.yandextaxitech:binaryprefs:1.0.0"
}