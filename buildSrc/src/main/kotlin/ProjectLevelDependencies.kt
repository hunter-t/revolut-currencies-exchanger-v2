import GradlePluginsVersions.GRADLE_PLUGIN
import GradlePluginsVersions.KOTLIN_PLUGIN

/**
 * Created by Ruslan Arslanov on 15/02/2020.
 */

internal object GradlePluginsVersions {
    const val GRADLE_PLUGIN = "3.6.0"
    const val KOTLIN_PLUGIN = "1.3.61"
}

object GradlePlugins {
    const val GRADLE = "com.android.tools.build:gradle:$GRADLE_PLUGIN"
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:$KOTLIN_PLUGIN"
}