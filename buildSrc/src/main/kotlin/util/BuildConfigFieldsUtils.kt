package util

import com.android.build.gradle.internal.dsl.BaseFlavor

/**
 * Created by Ruslan Arslanov on 15/02/2020.
 */

fun BaseFlavor.stringConfigField(name: String, value: String) {
    buildConfigField("String", name, "\"$value\"")
}

fun BaseFlavor.booleanConfigField(name: String, value: Boolean) {
    buildConfigField("boolean", name, value.toString())
}

fun BaseFlavor.longConfigField(name: String, value: Long) {
    buildConfigField("Long", name, value.toString())
}

fun BaseFlavor.intConfigField(name: String, value: Int) {
    buildConfigField("Integer", name, value.toString())
}

fun BaseFlavor.floatConfigField(name: String, value: Float) {
    buildConfigField("Float", name, value.toString())
}