package constants

import model.SdkApiVersion

/**
 * Created by Ruslan Arslanov on 15/02/2020.
 */
object SdkVersions {

    val MIN_SDK_VERSION = SdkApiVersion(21)

    val TARGET_SDK_VERSION = SdkApiVersion(29)

    val COMPILE_SDK_VERSION = TARGET_SDK_VERSION.sdkVersion

}