package constants

/**
 * Created by Ruslan Arslanov on 16/02/2020.
 */
object ApplicationConstants {

    const val APP_VERSION_CODE = 1

    const val APP_VERSION_NAME = "1.0.0"

    const val APP_ID = "com.revolut.exchanger"

    const val API_BASE_URL = "https://hiring.revolut.codes/api/android/"

}