import buildconfig.AppModuleBuildConfiguration

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-android-extensions")
}

android {
    AppModuleBuildConfiguration(project, androidExtension = this).configure()
}

androidExtensions {
    isExperimental = true
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Modules.
    implementation(project(":utils"))

    // Kotlin.
    implementation(Kotlin.STDLIB)

    // Rx.
    implementation(Rx.RX_JAVA)
    implementation(Rx.RX_KOTLIN)
    implementation(Rx.RX_ANDROID)
    implementation(Rx.RX_RELAY)

    // AndroidX.
    implementation(AndroidX.CORE)
    implementation(AndroidX.CORE_UI)
    implementation(AndroidX.CORE_UTILS)
    implementation(AndroidX.FRAGMENT)
    implementation(AndroidX.APPCOMPAT)
    implementation(AndroidX.ANNOTATIONS)
    implementation(AndroidX.CONSTRAINT_LAYOUT)
    implementation(MaterialComponents.LIBRARY)

    // Navigation.
    implementation(Navigation.UI)
    implementation(Navigation.FRAGMENT)

    // Dagger2.
    implementation(Dagger2.LIBRARY)
    implementation(Dagger2.ANDROID_EXTENSIONS)
    implementation(Dagger2.SUPPORT_EXTENSIONS)
    kapt(Dagger2.COMPILER)
    kapt(Dagger2.ANDROID_EXTENSIONS_COMPILER)
    implementation(Dagger2.ANNOTATIONS)

    // Retrofit.
    implementation(Retrofit.LIBRARY)
    implementation(Retrofit.RX_ADAPTER)
    implementation(Retrofit.GSON_CONVERTER)

    // Lifecycle.
    implementation(LifeCycle.VIEW_MODEL_LIBRARY)
    kapt(LifeCycle.VIEW_MODEL_COMPILER)
    implementation(LifeCycle.LIFECYCLE_EXTENSIONS)

    // Tools.
    implementation(Tools.TIMBER)
    implementation(Tools.ADAPTER_DELEGATES)
    implementation(Tools.OKHTTP_LOGGER) {
        exclude(group = "org.json", module = "json")
    }
}