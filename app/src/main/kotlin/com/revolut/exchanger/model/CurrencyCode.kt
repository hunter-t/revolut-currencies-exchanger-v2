package com.revolut.exchanger.model

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
inline class CurrencyCode(val value: String)