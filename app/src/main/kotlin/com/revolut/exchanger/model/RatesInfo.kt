package com.revolut.exchanger.model

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
data class RatesInfo(
    val rates: List<Rate>,
    val baseCurrency: CurrencyCode
)