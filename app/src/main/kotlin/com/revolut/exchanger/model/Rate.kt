package com.revolut.exchanger.model

import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
data class Rate(
    val value: BigDecimal,
    val currencyCode: CurrencyCode
)