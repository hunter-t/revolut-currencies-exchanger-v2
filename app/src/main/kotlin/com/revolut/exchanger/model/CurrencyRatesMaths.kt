package com.revolut.exchanger.model

import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Created by Ruslan Arslanov on 25/02/2020.
 */

private const val NORMAL_SCALE: Int = 2
private const val DIVISION_SCALE: Int = 10

internal fun BigDecimal.normalize(): BigDecimal {
    return setScale(NORMAL_SCALE, RoundingMode.HALF_UP)
}

internal fun BigDecimal.multiplyBy(multiplier: BigDecimal): BigDecimal {
    return multiply(multiplier)
}

internal fun BigDecimal.divideBy(divider: BigDecimal): BigDecimal {
    return divide(divider, DIVISION_SCALE, RoundingMode.HALF_UP)
}
