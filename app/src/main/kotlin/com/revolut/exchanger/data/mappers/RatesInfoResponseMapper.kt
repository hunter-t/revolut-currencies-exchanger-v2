package com.revolut.exchanger.data.mappers

import com.revolut.exchanger.data.api.RatesInfoResponseBody
import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.model.Rate
import com.revolut.exchanger.model.RatesInfo
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
class RatesInfoResponseMapper {

    fun map(responseBody: RatesInfoResponseBody): RatesInfo {
        return RatesInfo(
            rates = responseBody.rates.map(::mapRate),
            baseCurrency = CurrencyCode(responseBody.baseCurrency)
        )
    }

    private fun mapRate(rateEntry: Map.Entry<String, Double>): Rate {
        val (currencyCode: String, rate: Double) = rateEntry
        return Rate(
            value = BigDecimal(rate),
            currencyCode = CurrencyCode(currencyCode)
        )
    }

}