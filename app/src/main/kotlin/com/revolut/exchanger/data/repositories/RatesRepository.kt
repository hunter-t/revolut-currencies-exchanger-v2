package com.revolut.exchanger.data.repositories

import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.model.RatesInfo
import io.reactivex.Single

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
interface RatesRepository {

    fun getRatesInfo(baseCurrencyCode: CurrencyCode? = null): Single<RatesInfo>

}