package com.revolut.exchanger.data.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
interface RatesInfoApiService {

    @GET("latest")
    fun getRatesInfo(
        @Query("base") baseCurrency: String?
    ): Single<RatesInfoResponseBody>

}