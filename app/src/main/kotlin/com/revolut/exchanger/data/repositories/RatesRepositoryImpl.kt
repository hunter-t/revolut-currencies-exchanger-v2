package com.revolut.exchanger.data.repositories

import com.revolut.exchanger.data.api.RatesInfoApiService
import com.revolut.exchanger.data.mappers.RatesInfoResponseMapper
import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.model.RatesInfo
import io.reactivex.Single

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
class RatesRepositoryImpl(
    private val apiService: RatesInfoApiService,
    private val mapper: RatesInfoResponseMapper
) : RatesRepository {

    override fun getRatesInfo(baseCurrencyCode: CurrencyCode?): Single<RatesInfo> {
        return apiService
            .getRatesInfo(baseCurrencyCode?.value)
            .map(mapper::map)
    }

}