package com.revolut.exchanger.data.api

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
class RatesInfoResponseBody(
    val baseCurrency: String,
    val rates: Map<String, Double>
)