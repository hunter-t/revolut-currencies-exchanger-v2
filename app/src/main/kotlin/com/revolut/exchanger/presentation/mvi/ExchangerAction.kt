package com.revolut.exchanger.presentation.mvi

import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.model.RatesInfo
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
sealed class ExchangerAction {

    class UpdateRatesAction(
        val baseCurrency: CurrencyCode
    ) : ExchangerAction()

    class SelectBaseCurrencyAction(
        val amount: BigDecimal,
        val baseCurrency: CurrencyCode
    ) : ExchangerAction()

    class CalculateAmountsAction(
        val amount: BigDecimal,
        val baseCurrency: CurrencyCode
    ) : ExchangerAction()

    object RetryLoadingAction : ExchangerAction()

    object ConnectionStateRestored : ExchangerAction()

    class RatesLoadingSuccessAction(val ratesInfo: RatesInfo) : ExchangerAction()

    class RatesLoadingFailureAction(val throwable: Throwable) : ExchangerAction()

}