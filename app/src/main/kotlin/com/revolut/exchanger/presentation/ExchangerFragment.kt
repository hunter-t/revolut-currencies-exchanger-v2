package com.revolut.exchanger.presentation

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.revolut.exchanger.R
import com.revolut.exchanger.presentation.adapter.ExchangerAdapter
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.presentation.viewmodel.ExchangerViewModel
import com.revolut.exchanger.utils.extensions.doOnItemCountChanged
import com.revolut.exchanger.utils.extensions.doOnItemRangeMoved
import com.revolut.exchanger.utils.extensions.doOnScroll
import com.revolut.exchanger.utils.extensions.doOnTouchAndMove
import com.revolut.exchanger.utils.extensions.hideKeyboard
import com.revolut.exchanger.utils.extensions.switchScrollability
import com.revolut.exchanger.utils.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_exchanger.*
import javax.inject.Inject


/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
class ExchangerFragment : BaseFragment() {

    @Inject lateinit var viewModel: ExchangerViewModel
    private lateinit var recyclerAdapter: ExchangerAdapter

    override val layoutResId: Int = R.layout.fragment_exchanger

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupRecyclerView()
        startStateObserving()
    }

    private fun setupRecyclerView() {
        recyclerAdapter = ExchangerAdapter(
            onRetryClicked = viewModel::retry,
            onAmountChanged = viewModel::setAmount,
            onCurrencySelected = viewModel::selectBaseCurrency
        )
        ratesRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = recyclerAdapter
            doOnScroll { _, _ ->
                activateAppBar(canScrollVertically(-1))
            }
            doOnTouchAndMove {
                hideKeyboard()
            }
        }
        recyclerAdapter.apply {
            doOnItemRangeMoved {
                ratesRecyclerView.layoutManager?.scrollToPosition(0)
            }
            doOnItemCountChanged { itemCount ->
                toolbar.switchScrollability(itemCount > 1)
            }
        }
    }

    private fun startStateObserving() {
        viewModel
            .exchangerState
            .observe(::handleState)
    }

    private fun handleState(state: ExchangerState) {
        recyclerAdapter.items = state.adapterCells
        updateErrorSnack(
            updateError = state.updateError,
            noConnection = state.noConnection,
            ratesExist = state.rates.isNotEmpty()
        )
    }

    private fun updateErrorSnack(
        ratesExist: Boolean,
        updateError: Boolean,
        noConnection: Boolean
    ) {
        when {
            ratesExist.not() -> {
                dismissSnack()
            }
            noConnection -> {
                showIndefiniteSnack(R.string.snack_no_connection)
            }
            updateError -> {
                showIndefiniteSnack(
                    messageResId = R.string.snack_failed_to_update_rates,
                    actionResId = R.string.snack_retry_button
                ) {
                    viewModel.retry()
                }
            }
            else -> dismissSnack()
        }
    }

    private fun activateAppBar(show: Boolean) {
        appBarLayout.isActivated = show
    }

}