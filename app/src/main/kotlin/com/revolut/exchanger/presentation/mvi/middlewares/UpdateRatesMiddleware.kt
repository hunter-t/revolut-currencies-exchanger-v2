package com.revolut.exchanger.presentation.mvi.middlewares

import com.revolut.exchanger.data.repositories.RatesRepository
import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.presentation.mvi.ExchangerAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RatesLoadingFailureAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RatesLoadingSuccessAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.SelectBaseCurrencyAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.UpdateRatesAction
import com.revolut.exchanger.utils.mvi.Middleware
import com.revolut.exchanger.utils.rx.SchedulersFacade
import io.reactivex.Observable

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
class UpdateRatesMiddleware(
    private val schedulers: SchedulersFacade,
    private val ratesRepository: RatesRepository
) : Middleware<ExchangerAction> {

    override fun bind(actions: Observable<ExchangerAction>): Observable<ExchangerAction> {
        return actions
            .filter { action ->
                action is UpdateRatesAction || action is SelectBaseCurrencyAction
            }
            .flatMap { action ->
                val baseCurrency = extractBaseCurrency(action)
                if (baseCurrency != null) {
                    buildUpdateObservable(baseCurrency)
                } else {
                    buildNullCurrencyErrorObservable()
                }
            }
    }

    private fun extractBaseCurrency(action: ExchangerAction): CurrencyCode? {
        return when (action) {
            is UpdateRatesAction -> action.baseCurrency
            is SelectBaseCurrencyAction -> action.baseCurrency
            else -> null
        }
    }

    private fun buildUpdateObservable(baseCurrency: CurrencyCode): Observable<ExchangerAction> {
        return ratesRepository
            .getRatesInfo(baseCurrency)
            .map<ExchangerAction>(::RatesLoadingSuccessAction)
            .onErrorReturn(::RatesLoadingFailureAction)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .toObservable()
    }

    private fun buildNullCurrencyErrorObservable(): Observable<ExchangerAction> {
        val error = RatesLoadingFailureAction(
            IllegalStateException("Failed to extract currency from action")
        )
        return Observable.just(error)
    }

}