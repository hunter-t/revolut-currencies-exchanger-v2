package com.revolut.exchanger.presentation.adapter.delegates

import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.presentation.adapter.cells.ExchangerRateCell
import com.revolut.exchanger.utils.adapter.delegates.BaseCellDelegate
import com.revolut.exchanger.utils.adapter.viewholders.BaseCellViewHolder
import com.revolut.exchanger.utils.extensions.doOnFocusReceived
import com.revolut.exchanger.utils.extensions.doWithIgnoringChanges
import com.revolut.exchanger.utils.extensions.isIgnoringChanges
import com.revolut.exchanger.utils.extensions.moveCursorToEnd
import com.revolut.exchanger.utils.extensions.onClick
import com.revolut.exchanger.utils.extensions.showKeyboard
import kotlinx.android.synthetic.main.cell_exchanger_rate.*
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
class ExchangerRateCellDelegate(
    private val onAmountChanged: (amount: BigDecimal, currencyCode: CurrencyCode) -> Unit,
    private val onCurrencySelected: (amount: BigDecimal, currencyCode: CurrencyCode) -> Unit
) : BaseCellDelegate<ExchangerRateCell>(
    ExchangerRateCell.VIEW_TYPE
) {

    object RatePayload

    override fun onBindCell(cell: ExchangerRateCell, viewHolder: BaseCellViewHolder) {
        with(viewHolder) {
            currencyIconImageView.setImageResource(cell.iconResId)
            currencyCodeTextView.text = cell.currencyCode
            currencyNameTextView.text = cell.currencyName
            setupAmountEditText(cell, amountEditText)
            itemView.onClick {
                amountEditText.showKeyboard()
                dispatchBaseCurrencySelection(cell, amountEditText)
            }
        }
    }

    override fun onUpdateCell(
        cell: ExchangerRateCell,
        viewHolder: BaseCellViewHolder,
        payloads: MutableList<Any>
    ) {
        payloads.forEach { payload ->
            if (payload is RatePayload && cell.isBaseCurrency.not()) {
                onBindCell(cell, viewHolder)
            }
        }
    }

    private fun setupAmountEditText(cell: ExchangerRateCell, amountEditText: EditText) {
        amountEditText.apply {
            doWithIgnoringChanges {
                setText(cell.amount)
            }
            doOnFocusReceived {
                dispatchBaseCurrencySelection(cell, amountEditText)
                amountEditText.moveCursorToEnd()
            }
            if (hasFocus()) {
                amountEditText.moveCursorToEnd()
            }
            doOnTextChanged { _, _, _, _ ->
                if (isIgnoringChanges) {
                    return@doOnTextChanged
                }
                dispatchAmountChanged(cell, amountEditText)
            }
        }
    }

    private fun dispatchBaseCurrencySelection(
        cell: ExchangerRateCell,
        amountEditText: EditText
    ) {
        val amount = extractAmount(amountEditText)
        onCurrencySelected(amount, CurrencyCode(cell.currencyCode))
    }

    private fun dispatchAmountChanged(
        cell: ExchangerRateCell,
        amountEditText: EditText
    ) {
        val amount = extractAmount(amountEditText)
        onAmountChanged(amount, CurrencyCode(cell.currencyCode))
    }

    private fun extractAmount(amountEditText: EditText): BigDecimal {
        return try {
            amountEditText.text.toString().toBigDecimal()
        } catch (exc: NumberFormatException) {
            BigDecimal.ZERO
        }
    }

}