package com.revolut.exchanger.presentation.mvi.reducer.handlers

import com.github.ajalt.timberkt.Timber
import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.model.Rate
import com.revolut.exchanger.model.divideBy
import com.revolut.exchanger.presentation.adapter.cells.ExchangerCellsBuilder
import com.revolut.exchanger.presentation.mvi.ExchangerAction.SelectBaseCurrencyAction
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.mvi.ActionHandler
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 25/02/2020.
 */
class SelectBaseCurrencyActionHandler(
    private val cellsBuilder: ExchangerCellsBuilder
) : ActionHandler<SelectBaseCurrencyAction, ExchangerState> {

    override fun handle(
        state: ExchangerState,
        action: SelectBaseCurrencyAction
    ): ExchangerState {
        val selectedAmount = action.amount
        val selectedBaseCurrency = action.baseCurrency
        val updatedRates = updateRatesList(
            currentRates = state.rates,
            selectedBaseCurrency = selectedBaseCurrency
        )
        val newCells = cellsBuilder.buildRateCells(
            amount = selectedAmount,
            rates = updatedRates,
            baseCurrency = selectedBaseCurrency
        )
        return state.copy(
            rates = updatedRates,
            amount = selectedAmount,
            adapterCells = newCells,
            baseCurrency = selectedBaseCurrency
        )
    }

    /**
     * Pull selected Base Currency to the top of the list and re-calculate
     * all rates relatively to the new Base Currency.
     */
    private fun updateRatesList(
        currentRates: List<Rate>,
        selectedBaseCurrency: CurrencyCode
    ): List<Rate> {
        val selectedRate = currentRates.find { it.currencyCode == selectedBaseCurrency }
        if (selectedRate == null) {
            Timber.e { "Failed to find selected rate by currency" }
            return currentRates
        }
        return currentRates
            .asSequence()
            .filter { it != selectedRate }
            .map { updatedRate ->
                updateRateRelativelyToSelectedBaseCurrency(
                    updatedRate = updatedRate,
                    selectedRate = selectedRate
                )
            }
            .toMutableList()
            .apply {
                val updatedSelectedRate = Rate(
                    value = BigDecimal.ONE,
                    currencyCode = selectedRate.currencyCode
                )
                add(0, updatedSelectedRate)
            }
    }

    /**
     * Update every rate in list relatively to the selected Base Currency.
     */
    private fun updateRateRelativelyToSelectedBaseCurrency(
        updatedRate: Rate,
        selectedRate: Rate
    ): Rate {
        val relativeRate = if (selectedRate.value != BigDecimal.ZERO) {
            updatedRate.value.divideBy(selectedRate.value)
        } else {
            BigDecimal.ZERO
        }
        return Rate(
            value = relativeRate,
            currencyCode = updatedRate.currencyCode
        )
    }

}