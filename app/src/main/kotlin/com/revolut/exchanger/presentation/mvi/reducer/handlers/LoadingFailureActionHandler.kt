package com.revolut.exchanger.presentation.mvi.reducer.handlers

import com.github.ajalt.timberkt.Timber
import com.revolut.exchanger.presentation.adapter.cells.ExchangerCellsBuilder
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RatesLoadingFailureAction
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.connection.ConnectionChecker
import com.revolut.exchanger.utils.mvi.ActionHandler

/**
 * Created by Ruslan Arslanov on 26/02/2020.
 */
class LoadingFailureActionHandler(
    private val cellsBuilder: ExchangerCellsBuilder,
    private val connectionChecker: ConnectionChecker
) : ActionHandler<RatesLoadingFailureAction, ExchangerState> {

    override fun handle(
        state: ExchangerState,
        action: RatesLoadingFailureAction
    ): ExchangerState {
        if (state.updateError || state.noConnection) {
            return state
        }
        Timber.e(action.throwable) { "Failed to load rates" }
        val hasRates = state.rates.isNotEmpty()
        return if (connectionChecker.isConnected()) {
            buildLoadingErrorState(hasRates, state)
        } else {
            buildNoConnectionState(hasRates, state)
        }
    }

    private fun buildNoConnectionState(
        hasRates: Boolean,
        currentState: ExchangerState
    ): ExchangerState {
        val newAdapterCells = if (hasRates) {
            currentState.adapterCells
        } else {
            listOf(cellsBuilder.buildNoConnectionZeroDataCell())
        }
        return currentState.copy(
            noConnection = true,
            updateError = false,
            adapterCells = newAdapterCells
        )
    }

    private fun buildLoadingErrorState(
        hasRates: Boolean,
        currentState: ExchangerState
    ): ExchangerState {
        val newAdapterCells = if (hasRates) {
            currentState.adapterCells
        } else {
            listOf(cellsBuilder.buildLoadingErrorZeroDataCell())
        }
        return currentState.copy(
            updateError = true,
            noConnection = false,
            adapterCells = newAdapterCells
        )
    }

}