package com.revolut.exchanger.presentation.adapter.delegates

import androidx.core.view.doOnPreDraw
import com.revolut.exchanger.presentation.adapter.cells.ZeroDataCell
import com.revolut.exchanger.utils.adapter.delegates.BaseCellDelegate
import com.revolut.exchanger.utils.adapter.viewholders.BaseCellViewHolder
import com.revolut.exchanger.utils.extensions.dipToPix
import com.revolut.exchanger.utils.extensions.adjustHeightToFillParent
import com.revolut.exchanger.utils.extensions.onClick
import kotlinx.android.synthetic.main.cell_zero_data.*


/**
 * Created by Ruslan Arslanov on 08.02.2019.
 */
class ZeroDataCellDelegate(
    private val fillParent: Boolean = true,
    private val minHeightDp: Int = ZeroDataCell.DEFAULT_MIN_HEIGHT_PD,
    private val onButtonClickListener: () -> Unit

) : BaseCellDelegate<ZeroDataCell>(
    ZeroDataCell.VIEW_TYPE

) {

    override fun onViewHolderCreated(viewHolder: BaseCellViewHolder) {
        viewHolder.zeroDataView.button.onClick(onButtonClickListener)
    }

    override fun onBindCell(cell: ZeroDataCell, viewHolder: BaseCellViewHolder) {
        with(viewHolder) {
            itemView.minimumHeight = itemView.context.dipToPix(minHeightDp)
            zeroDataView.setup(
                cell.iconRes,
                cell.titleRes,
                cell.contentMessageRes,
                cell.buttonTextRes
            )
            if (fillParent) {
                itemView.doOnPreDraw { view ->
                    view.adjustHeightToFillParent()
                }
            }
        }
    }

}