package com.revolut.exchanger.presentation.mvi.reducer.handlers

import com.revolut.exchanger.presentation.adapter.cells.ExchangerCellsBuilder
import com.revolut.exchanger.presentation.mvi.ExchangerAction.CalculateAmountsAction
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.mvi.ActionHandler

/**
 * Created by Ruslan Arslanov on 25/02/2020.
 */
class CalculateAmountActionHandler(
    private val cellsBuilder: ExchangerCellsBuilder
) : ActionHandler<CalculateAmountsAction, ExchangerState> {

    override fun handle(
        state: ExchangerState,
        action: CalculateAmountsAction
    ): ExchangerState {
        val newAmount = action.amount
        val newCells = cellsBuilder.buildRateCells(
            amount = newAmount,
            rates = state.rates,
            baseCurrency = action.baseCurrency
        )
        return state.copy(
            amount = newAmount,
            adapterCells = newCells
        )
    }

}