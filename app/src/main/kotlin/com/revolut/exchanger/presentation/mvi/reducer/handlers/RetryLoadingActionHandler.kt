package com.revolut.exchanger.presentation.mvi.reducer.handlers

import com.revolut.exchanger.presentation.adapter.cells.ScreenLoadingCell
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RetryLoadingAction
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.mvi.ActionHandler

/**
 * Created by Ruslan Arslanov on 27/02/2020.
 */
class RetryLoadingActionHandler : ActionHandler<RetryLoadingAction, ExchangerState> {

    override fun handle(state: ExchangerState, action: RetryLoadingAction): ExchangerState {
        val newCells = if (state.rates.isEmpty()) {
            listOf(ScreenLoadingCell)
        } else {
            state.adapterCells
        }
        return state.copy(
            updateError = false,
            noConnection = false,
            adapterCells = newCells
        )
    }

}