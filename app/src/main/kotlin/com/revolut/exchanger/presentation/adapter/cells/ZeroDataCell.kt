package com.revolut.exchanger.presentation.adapter.cells

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.revolut.exchanger.R
import com.revolut.exchanger.utils.adapter.cells.BaseCell

/**
 * Created by Ruslan Arslanov on 05/10/2018.
 */
class ZeroDataCell(
    @StringRes val titleRes: Int,
    @DrawableRes val iconRes: Int,
    @StringRes val contentMessageRes: Int? = null,
    // Pass null if you don't want to show button.
    @StringRes val buttonTextRes: Int? = null
) : BaseCell {

    companion object {
        const val VIEW_TYPE: Int = R.layout.cell_zero_data
        const val DEFAULT_MIN_HEIGHT_PD: Int = 300
    }

    override val cellLayout: Int = VIEW_TYPE

}