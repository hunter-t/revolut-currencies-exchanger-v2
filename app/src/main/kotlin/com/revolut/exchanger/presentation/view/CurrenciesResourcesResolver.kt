package com.revolut.exchanger.presentation.view

import com.revolut.exchanger.presentation.adapter.cells.ExchangerRateCell
import com.revolut.exchanger.utils.extensions.EMPTY_STRING
import com.revolut.exchanger.utils.extensions.capitalize
import com.revolut.exchanger.utils.resources.ResourcesManager
import java.util.Currency
import java.util.Locale

/**
 * Created by Ruslan Arslanov on 24/02/2020.
 */
class CurrenciesResourcesResolver(
    private val resourcesManager: ResourcesManager
) {

    private companion object {
        const val MISSING_ICON_RES_ID = 0
        const val ICON_RES_ID_TYPE = "drawable"
        const val PREFIX_ICON_RES_ID_NAME = "ic_"
    }

    private val currenciesNamesCache: MutableMap<String, String> = mutableMapOf()
    private val currenciesIconsResIdsCache: MutableMap<String, Int> = mutableMapOf()

    fun getCurrencyName(currencyCode: String): String {
        return currenciesNamesCache[currencyCode] ?: buildCurrencyName(currencyCode)
    }

    fun getCurrencyIconResId(currencyCode: String): Int {
        return currenciesIconsResIdsCache[currencyCode] ?: buildIconResId(currencyCode)
    }

    private fun buildCurrencyName(currencyCode: String): String {
        return Currency
            .getInstance(currencyCode)
            ?.displayName
            ?.capitalize()
            ?.also { currencyName ->
                currenciesNamesCache[currencyCode] = currencyName
            }
            ?: EMPTY_STRING
    }

    private fun buildIconResId(currencyCode: String): Int {
        val formattedCurrency = currencyCode.toLowerCase(Locale.getDefault())
        val iconName = "$PREFIX_ICON_RES_ID_NAME$formattedCurrency"
        resourcesManager
            .getIdentifier(
                name = iconName,
                type = ICON_RES_ID_TYPE
            )
            .let { iconResId ->
                return if (iconResId != MISSING_ICON_RES_ID) {
                    currenciesIconsResIdsCache[currencyCode] = iconResId
                    iconResId
                } else {
                    ExchangerRateCell.NO_ICON_RES_ID
                }
            }
    }

}