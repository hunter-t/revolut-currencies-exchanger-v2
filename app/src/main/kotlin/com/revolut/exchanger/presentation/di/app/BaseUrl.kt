package com.revolut.exchanger.presentation.di.app

import javax.inject.Qualifier

/**
 * Created by Ruslan Arslanov on 16/02/2020.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class BaseUrl