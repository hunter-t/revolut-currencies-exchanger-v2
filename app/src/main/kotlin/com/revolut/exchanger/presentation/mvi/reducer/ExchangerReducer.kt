package com.revolut.exchanger.presentation.mvi.reducer

import com.revolut.exchanger.presentation.mvi.ExchangerAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.CalculateAmountsAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.ConnectionStateRestored
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RatesLoadingFailureAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RatesLoadingSuccessAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RetryLoadingAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.SelectBaseCurrencyAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.UpdateRatesAction
import com.revolut.exchanger.presentation.mvi.reducer.handlers.CalculateAmountActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.ConnectionRestoredActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.LoadingFailureActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.LoadingSuccessActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.RetryLoadingActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.SelectBaseCurrencyActionHandler
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.mvi.Reducer

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
class ExchangerReducer(
    private val retryLoadingActionHandler: RetryLoadingActionHandler,
    private val loadingSuccessActionHandler: LoadingSuccessActionHandler,
    private val loadingFailureActionHandler: LoadingFailureActionHandler,
    private val calculateAmountActionHandler: CalculateAmountActionHandler,
    private val selectBaseCurrencyActionHandler: SelectBaseCurrencyActionHandler,
    private val connectionRestoredActionHandler: ConnectionRestoredActionHandler
) : Reducer<ExchangerAction, ExchangerState> {

    override fun reduce(state: ExchangerState, action: ExchangerAction): ExchangerState {
        return when (action) {
            is UpdateRatesAction -> state
            is RetryLoadingAction ->  retryLoadingActionHandler.handle(state, action)
            is CalculateAmountsAction -> calculateAmountActionHandler.handle(state, action)
            is RatesLoadingSuccessAction -> loadingSuccessActionHandler.handle(state, action)
            is RatesLoadingFailureAction -> loadingFailureActionHandler.handle(state, action)
            is ConnectionStateRestored -> connectionRestoredActionHandler.handle(state, action)
            is SelectBaseCurrencyAction -> selectBaseCurrencyActionHandler.handle(state, action)
        }
    }

}