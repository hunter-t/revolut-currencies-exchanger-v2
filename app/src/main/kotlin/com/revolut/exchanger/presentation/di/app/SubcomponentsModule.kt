package com.revolut.exchanger.presentation.di.app

import com.revolut.exchanger.presentation.ExchangerFragment
import com.revolut.exchanger.presentation.MainActivity
import com.revolut.exchanger.presentation.di.exchanger.ExchangerModule
import com.revolut.exchanger.presentation.di.exchanger.ExchangerScope
import com.revolut.exchanger.presentation.di.main.MainScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Ruslan Arslanov on 16/02/2020.
 */
@Module
abstract class SubcomponentsModule {

    @MainScope
    @ContributesAndroidInjector
    abstract fun buildMainActivity(): MainActivity

    @ExchangerScope
    @ContributesAndroidInjector(modules = [ExchangerModule::class])
    abstract fun buildExchangerFragment(): ExchangerFragment

}