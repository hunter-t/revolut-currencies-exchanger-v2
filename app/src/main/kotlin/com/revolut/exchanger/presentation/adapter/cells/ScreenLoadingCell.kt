package com.revolut.exchanger.presentation.adapter.cells

import com.revolut.exchanger.R
import com.revolut.exchanger.utils.adapter.cells.BaseCell


/**
 * Created by Ruslan Arslanov on 11.02.2019.
 */
object ScreenLoadingCell : BaseCell {

    const val VIEW_TYPE = R.layout.cell_screen_loading

    override val cellLayout: Int = VIEW_TYPE

}