package com.revolut.exchanger.presentation.mvi

import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.presentation.adapter.cells.ScreenLoadingCell
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.mvi.Store
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 25/02/2020.
 */
object ExchangerDefaultsFactory {

    private val DEFAULT_AMOUNT = BigDecimal.ONE
    private const val DEFAULT_BASE_CURRENCY_CODE = "EUR"

    fun buildDefaultStoreConfig(): Store.InitialConfig<ExchangerAction, ExchangerState> {
        return Store.InitialConfig(
            initState = buildInitialState(),
            initAction = buildInitialAction()
        )
    }

    private fun buildInitialState(): ExchangerState {
        return ExchangerState(
            rates = emptyList(),
            updateError = false,
            noConnection = false,
            amount = DEFAULT_AMOUNT,
            adapterCells = listOf(ScreenLoadingCell),
            baseCurrency = CurrencyCode(DEFAULT_BASE_CURRENCY_CODE)
        )
    }

    private fun buildInitialAction(): ExchangerAction {
        return ExchangerAction.UpdateRatesAction(
            CurrencyCode(DEFAULT_BASE_CURRENCY_CODE)
        )
    }

}