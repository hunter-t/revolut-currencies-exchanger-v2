package com.revolut.exchanger.presentation.mvi.reducer.handlers

import com.revolut.exchanger.presentation.adapter.cells.ScreenLoadingCell
import com.revolut.exchanger.presentation.mvi.ExchangerAction.ConnectionStateRestored
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.mvi.ActionHandler

/**
 * Created by Ruslan Arslanov on 26/02/2020.
 */
class ConnectionRestoredActionHandler : ActionHandler<ConnectionStateRestored, ExchangerState> {

    override fun handle(
        state: ExchangerState,
        action: ConnectionStateRestored
    ): ExchangerState {
        val newAdapterCells = if (state.rates.isNotEmpty()) {
            state.adapterCells
        } else {
            listOf(ScreenLoadingCell)
        }
        return state.copy(
            updateError = false,
            noConnection = false,
            adapterCells = newAdapterCells
        )
    }

}