package com.revolut.exchanger.presentation.mvi.states

import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.model.Rate
import com.revolut.exchanger.utils.adapter.cells.BaseCell
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
class ExchangerState(
    val rates: List<Rate>,
    val amount: BigDecimal,
    val updateError: Boolean,
    val noConnection: Boolean,
    val baseCurrency: CurrencyCode,
    val adapterCells: List<BaseCell>
) {

    /**
     * Completely re-create this state with modifying desirable properties.
     */
    fun copy(
        rates: List<Rate> = this.rates,
        amount: BigDecimal = this.amount,
        updateError: Boolean = this.updateError,
        noConnection: Boolean = this.noConnection,
        baseCurrency: CurrencyCode = this.baseCurrency,
        adapterCells: List<BaseCell> = this.adapterCells
    ): ExchangerState {
        return ExchangerState(
            rates = rates,
            amount = amount,
            updateError = updateError,
            noConnection = noConnection,
            baseCurrency = baseCurrency,
            adapterCells = adapterCells
        )
    }

}