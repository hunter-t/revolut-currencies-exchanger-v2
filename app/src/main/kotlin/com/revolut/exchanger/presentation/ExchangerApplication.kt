package com.revolut.exchanger.presentation

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment
import com.revolut.exchanger.BuildConfig
import com.revolut.exchanger.presentation.di.app.AppComponent
import com.revolut.exchanger.presentation.di.app.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Ruslan Arslanov on 16/02/2020.
 */
class ExchangerApplication : Application(),
    HasSupportFragmentInjector,
    HasActivityInjector {

    private lateinit var component: AppComponent

    @Inject lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<Fragment>
    @Inject lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        initLogger()
        initAppComponent()
    }

    private fun initAppComponent() {
        component = DaggerAppComponent
            .builder()
            .application(this)
            .build()
            .also { it.inject(this) }
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return dispatchingFragmentInjector
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingActivityInjector
    }

}