package com.revolut.exchanger.presentation.adapter

import com.revolut.exchanger.presentation.adapter.cells.ExchangerRateCell
import com.revolut.exchanger.presentation.adapter.delegates.ExchangerRateCellDelegate
import com.revolut.exchanger.utils.adapter.BaseDiffCallback
import com.revolut.exchanger.utils.adapter.cells.BaseCell

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */
class ExchangerDiffCallback : BaseDiffCallback() {

    override fun getChangePayload(oldItem: BaseCell, newItem: BaseCell): Any? {
        return if (oldItem is ExchangerRateCell && newItem is ExchangerRateCell) {
            ExchangerRateCellDelegate.RatePayload
        } else {
            super.getChangePayload(oldItem, newItem)
        }
    }

}