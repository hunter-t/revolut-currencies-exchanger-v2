package com.revolut.exchanger.presentation.di.app

import com.revolut.exchanger.presentation.ExchangerApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector

/**
 * Created by Ruslan Arslanov on 16/02/2020.
 */
@AppScope
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        SubcomponentsModule::class,
        AndroidInjectionModule::class
    ]
)
interface AppComponent : AndroidInjector<ExchangerApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: ExchangerApplication): Builder

        fun build(): AppComponent
    }

}