package com.revolut.exchanger.presentation.adapter

import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.presentation.adapter.delegates.ExchangerRateCellDelegate
import com.revolut.exchanger.presentation.adapter.delegates.ScreenLoadingCellDelegate
import com.revolut.exchanger.presentation.adapter.delegates.ZeroDataCellDelegate
import com.revolut.exchanger.utils.adapter.BaseCellDelegationAdapter
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 20/02/2020.
 */
class ExchangerAdapter(
    onRetryClicked: () -> Unit,
    onAmountChanged: (amount: BigDecimal, currencyCode: CurrencyCode) -> Unit,
    onCurrencySelected: (amount: BigDecimal, currencyCode: CurrencyCode) -> Unit
) : BaseCellDelegationAdapter(ExchangerDiffCallback()) {

    init {
        delegatesManager
            .addDelegate(
                ExchangerRateCellDelegate(
                    onAmountChanged = onAmountChanged,
                    onCurrencySelected = onCurrencySelected
                )
            )
            .addDelegate(
                ZeroDataCellDelegate(
                    fillParent = true,
                    onButtonClickListener = onRetryClicked
                )
            )
            .addDelegate(ScreenLoadingCellDelegate(fillParent = true))
    }

}