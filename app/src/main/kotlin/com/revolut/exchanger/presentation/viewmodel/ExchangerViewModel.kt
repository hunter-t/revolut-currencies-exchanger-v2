package com.revolut.exchanger.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.github.ajalt.timberkt.Timber
import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.presentation.mvi.ExchangerAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.ConnectionStateRestored
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RatesLoadingFailureAction
import com.revolut.exchanger.presentation.mvi.ExchangerAction.UpdateRatesAction
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.connection.ConnectionStateObserver
import com.revolut.exchanger.utils.extensions.asLiveData
import com.revolut.exchanger.utils.mvi.Store
import com.revolut.exchanger.utils.viewmodel.BaseViewModel
import io.reactivex.Observable
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
class ExchangerViewModel(
    private val store: Store<ExchangerAction, ExchangerState>,
    private val connectionStateObserver: ConnectionStateObserver
) : BaseViewModel() {

    private companion object {
        const val UPDATE_INTERVAL_SEC = 1L
    }

    private val state = MutableLiveData<ExchangerState>()
    val exchangerState: LiveData<ExchangerState> = state.asLiveData

    private val mustSkipRatesUpdates: Boolean
        get() {
            val currentState = state.value ?: return true
            return currentState.noConnection || currentState.updateError
        }

    init {
        store.wire(state).disposeOnCleared()
        setupRatesUpdate()
        setupConnectionObserver()
    }

    private fun setupRatesUpdate() {
        Observable
            .interval(UPDATE_INTERVAL_SEC, TimeUnit.SECONDS)
            .filter { mustSkipRatesUpdates.not() }
            .map {
                val currentState = state.value as ExchangerState
                UpdateRatesAction(currentState.baseCurrency)
            }
            .subscribe(store::postAction) { throwable ->
                store.postAction(RatesLoadingFailureAction(throwable))
            }
            .disposeOnCleared()
    }

    private fun setupConnectionObserver() {
        connectionStateObserver
            .observe()
            .filter { isConnected -> isConnected }
            .subscribe(
                {
                    store.postAction(ConnectionStateRestored)
                },
                Timber::e
            )
            .disposeOnCleared()
    }

    fun selectBaseCurrency(amount: BigDecimal, currencyCode: CurrencyCode) {
        val action = ExchangerAction.SelectBaseCurrencyAction(amount, currencyCode)
        store.postAction(action)
    }

    fun setAmount(amount: BigDecimal, currencyCode: CurrencyCode) {
        val action = ExchangerAction.CalculateAmountsAction(amount, currencyCode)
        store.postAction(action)
    }

    fun retry() {
        store.postAction(ExchangerAction.RetryLoadingAction)
    }

}