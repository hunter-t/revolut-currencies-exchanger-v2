package com.revolut.exchanger.presentation.di.app

import android.content.Context
import android.os.Build
import android.util.Base64
import com.github.ajalt.timberkt.d
import com.github.ajalt.timberkt.i
import com.github.ajalt.timberkt.w
import com.google.gson.Gson
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.revolut.exchanger.BuildConfig
import com.revolut.exchanger.utils.connection.ConnectionChecker
import com.revolut.exchanger.utils.connection.ConnectionCheckerImpl
import com.revolut.exchanger.utils.connection.ConnectionStateObserver
import com.revolut.exchanger.utils.connection.ConnectionStateObserverApi21Impl
import com.revolut.exchanger.utils.connection.ConnectionStateObserverApi23Impl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by Ruslan Arslanov on 16/02/2020.
 */
@Module
class NetworkModule {

    private companion object {
        const val READ_TIMEOUT_SECONDS = 30L
    }

    @Provides
    @AppScope
    fun provideOkHttpClient(
        loggingInterceptor: LoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .apply {
                if (BuildConfig.DEBUG) {
                    addInterceptor(loggingInterceptor)
                }
            }
            .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @AppScope
    fun provideLoggingInterceptor(): LoggingInterceptor {
        return LoggingInterceptor.Builder()
            .loggable(BuildConfig.DEBUG)
            .setLevel(Level.BASIC)
            .logger { level, tag, message ->
                when (level) {
                    Platform.INFO -> Timber.tag(tag).i { message }
                    Platform.WARN -> Timber.tag(tag).w { message }
                    else -> Timber.tag(tag).d { message }
                }
            }
            .build()
    }

    @Provides
    @AppScope
    fun provideRetrofit(
        gson: Gson,
        @BaseUrl baseUrl: String,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit
            .Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @AppScope
    fun provideGson(): Gson {
        return Gson()
    }

    @Provides
    @BaseUrl
    @AppScope
    fun provideBaseUrl(): String {
        return Base64
            .decode(BuildConfig.API_BASE_URL, Base64.DEFAULT)
            .toString(Charsets.UTF_8)
    }

    @Provides
    @AppScope
    fun provideConnectionChecker(
        context: Context
    ): ConnectionChecker {
        return ConnectionCheckerImpl(context)
    }

    @Provides
    @AppScope
    fun provideConnectionStateObserver(
        context: Context
    ): ConnectionStateObserver {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ConnectionStateObserverApi23Impl(context)
        } else {
            ConnectionStateObserverApi21Impl(context)
        }
    }

}