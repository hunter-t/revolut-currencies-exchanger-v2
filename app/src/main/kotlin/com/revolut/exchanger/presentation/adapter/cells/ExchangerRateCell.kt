package com.revolut.exchanger.presentation.adapter.cells

import androidx.annotation.DrawableRes
import com.revolut.exchanger.R
import com.revolut.exchanger.utils.adapter.cells.IdentifiableCell

/**
 * Created by Ruslan Arslanov on 20/02/2020.
 */
data class ExchangerRateCell(
    val amount: String,
    val currencyName: String,
    val currencyCode: String,
    val isBaseCurrency: Boolean,
    @DrawableRes val iconResId: Int = NO_ICON_RES_ID
): IdentifiableCell<String> {

    companion object {
        const val VIEW_TYPE = R.layout.cell_exchanger_rate
        const val NO_ICON_RES_ID = R.drawable.ic_empty_currency
    }

    override val cellLayout: Int = VIEW_TYPE

    override val cellId: String = currencyCode

}