package com.revolut.exchanger.presentation.adapter.cells

import com.revolut.exchanger.R
import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.model.Rate
import com.revolut.exchanger.model.multiplyBy
import com.revolut.exchanger.model.normalize
import com.revolut.exchanger.presentation.view.CurrenciesResourcesResolver
import com.revolut.exchanger.utils.adapter.cells.BaseCell
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 24/02/2020.
 */
class ExchangerCellsBuilder(
    private val resourcesResolver: CurrenciesResourcesResolver
) {

    fun buildRateCells(
        rates: List<Rate>,
        amount: BigDecimal,
        baseCurrency: CurrencyCode
    ): List<BaseCell> {
        return rates.map { rate ->
            if (rate.currencyCode == baseCurrency) {
                buildBaseRateCell(baseCurrency, amount)
            } else {
                buildRegularRateCell(rate, amount)
            }
        }
    }

    fun buildLoadingErrorZeroDataCell(): ZeroDataCell {
        return ZeroDataCell(
            iconRes = R.drawable.ic_zero_data_unspecified,
            titleRes = R.string.zero_data_title_loading_error,
            contentMessageRes = R.string.zero_data_message_loading_error,
            buttonTextRes = R.string.zero_data_retry_loading_error
        )
    }

    fun buildNoConnectionZeroDataCell(): ZeroDataCell {
        return ZeroDataCell(
            iconRes = R.drawable.ic_zero_data_no_connection,
            titleRes = R.string.zero_data_title_no_connection,
            contentMessageRes = R.string.zero_data_message_no_connection
        )
    }

    private fun buildRegularRateCell(rate: Rate, amount: BigDecimal): BaseCell {
        val calculatedAmount = rate
            .value
            .multiplyBy(amount)
            .normalize()
            .toString()
        return ExchangerRateCell(
            isBaseCurrency = false,
            amount = calculatedAmount,
            currencyCode = rate.currencyCode.value,
            currencyName = resourcesResolver.getCurrencyName(rate.currencyCode.value),
            iconResId = resourcesResolver.getCurrencyIconResId(rate.currencyCode.value)
        )
    }

    private fun buildBaseRateCell(baseCurrency: CurrencyCode, amount: BigDecimal): BaseCell {
        return ExchangerRateCell(
            isBaseCurrency = true,
            amount = amount.toString(),
            currencyCode = baseCurrency.value,
            currencyName = resourcesResolver.getCurrencyName(baseCurrency.value),
            iconResId = resourcesResolver.getCurrencyIconResId(baseCurrency.value)
        )
    }

}