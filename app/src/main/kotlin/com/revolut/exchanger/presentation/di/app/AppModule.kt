package com.revolut.exchanger.presentation.di.app

import android.content.Context
import com.revolut.exchanger.presentation.ExchangerApplication
import com.revolut.exchanger.utils.resources.ResourcesManager
import com.revolut.exchanger.utils.resources.ResourcesManagerImpl
import com.revolut.exchanger.utils.rx.SchedulersFacade
import com.revolut.exchanger.utils.rx.SchedulersFacadeImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Ruslan Arslanov on 16/02/2020.
 */
@Module
class AppModule {

    @Provides
    @AppScope
    fun provideContext(app: ExchangerApplication): Context {
        return app.applicationContext
    }

    @Provides
    @AppScope
    fun provideSchedulersFacade(): SchedulersFacade {
        return SchedulersFacadeImpl()
    }

    @Provides
    @AppScope
    fun provideResourcesManager(context: Context): ResourcesManager {
        return ResourcesManagerImpl(context)
    }

}