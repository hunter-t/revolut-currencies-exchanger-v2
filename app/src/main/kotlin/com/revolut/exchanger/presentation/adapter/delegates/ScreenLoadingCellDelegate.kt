package com.revolut.exchanger.presentation.adapter.delegates

import androidx.core.view.doOnPreDraw
import com.revolut.exchanger.presentation.adapter.cells.ScreenLoadingCell
import com.revolut.exchanger.utils.adapter.delegates.BaseCellDelegate
import com.revolut.exchanger.utils.adapter.viewholders.BaseCellViewHolder
import com.revolut.exchanger.utils.extensions.adjustHeightToFillParent

/**
 * Created by Ruslan Arslanov on 11.02.2019.
 */
class ScreenLoadingCellDelegate(
    private val fillParent: Boolean = true
) : BaseCellDelegate<ScreenLoadingCell>(ScreenLoadingCell.VIEW_TYPE) {

    override fun onBindCell(cell: ScreenLoadingCell, viewHolder: BaseCellViewHolder) {
        if (fillParent.not()) {
            return
        }
        with(viewHolder.itemView) {
            doOnPreDraw { view ->
                view.adjustHeightToFillParent()
            }
        }
    }

}