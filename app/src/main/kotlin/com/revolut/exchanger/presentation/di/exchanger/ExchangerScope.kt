package com.revolut.exchanger.presentation.di.exchanger

import javax.inject.Scope

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ExchangerScope