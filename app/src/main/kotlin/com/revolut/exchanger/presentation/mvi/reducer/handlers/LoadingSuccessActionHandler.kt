package com.revolut.exchanger.presentation.mvi.reducer.handlers

import com.revolut.exchanger.model.CurrencyCode
import com.revolut.exchanger.model.Rate
import com.revolut.exchanger.presentation.adapter.cells.ExchangerCellsBuilder
import com.revolut.exchanger.presentation.mvi.ExchangerAction.RatesLoadingSuccessAction
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.utils.mvi.ActionHandler
import java.math.BigDecimal

/**
 * Created by Ruslan Arslanov on 25/02/2020.
 */
class LoadingSuccessActionHandler(
    private val cellsBuilder: ExchangerCellsBuilder
) : ActionHandler<RatesLoadingSuccessAction, ExchangerState> {

    override fun handle(
        state: ExchangerState,
        action: RatesLoadingSuccessAction
    ): ExchangerState {
        return if (state.noConnection) {
            // Loaded rates were requested before connection was lost, so we must skip them.
            state
        } else {
            buildNewState(state, action)
        }
    }

    private fun buildNewState(
        state: ExchangerState,
        action: RatesLoadingSuccessAction
    ): ExchangerState {
        val oldRates = state.rates
        val newRates = action.ratesInfo.rates
        val newBaseCurrency = action.ratesInfo.baseCurrency
        val updatedRates = updateRatesList(
            oldRates = oldRates,
            newRates = newRates,
            baseCurrency = newBaseCurrency
        )
        val newCells = cellsBuilder.buildRateCells(
            rates = updatedRates,
            amount = state.amount,
            baseCurrency = newBaseCurrency
        )
        return state.copy(
            updateError = false,
            noConnection = false,
            rates = updatedRates,
            adapterCells = newCells,
            baseCurrency = newBaseCurrency
        )
    }

    private fun updateRatesList(
        oldRates: List<Rate>,
        newRates: List<Rate>,
        baseCurrency: CurrencyCode
    ): List<Rate> {
        if (oldRates.isEmpty()) {
            return putBaseCurrencyToTop(newRates, baseCurrency)
        }
        val missingRates = newRates.filter { newRate ->
            oldRates.none { it.currencyCode == newRate.currencyCode }
        }
        return oldRates
            .map { oldRate ->
                val updatedRate = newRates.find { newRate ->
                    newRate.currencyCode == oldRate.currencyCode
                }
                updatedRate ?: oldRate
            }
            .toMutableList()
            .apply {
                addAll(missingRates)
            }
    }

    private fun putBaseCurrencyToTop(
        rates: List<Rate>,
        baseCurrency: CurrencyCode
    ): List<Rate> {
        val baseCurrencyRate = Rate(
            value = BigDecimal.ONE,
            currencyCode = baseCurrency
        )
        return rates
            .toMutableList()
            .apply {
                add(0, baseCurrencyRate)
            }
    }

}