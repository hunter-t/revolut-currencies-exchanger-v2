package com.revolut.exchanger.presentation.di.exchanger

import com.revolut.exchanger.data.api.RatesInfoApiService
import com.revolut.exchanger.data.mappers.RatesInfoResponseMapper
import com.revolut.exchanger.data.repositories.RatesRepository
import com.revolut.exchanger.data.repositories.RatesRepositoryImpl
import com.revolut.exchanger.presentation.ExchangerFragment
import com.revolut.exchanger.presentation.adapter.cells.ExchangerCellsBuilder
import com.revolut.exchanger.presentation.mvi.ExchangerAction
import com.revolut.exchanger.presentation.mvi.ExchangerDefaultsFactory
import com.revolut.exchanger.presentation.mvi.middlewares.UpdateRatesMiddleware
import com.revolut.exchanger.presentation.mvi.reducer.ExchangerReducer
import com.revolut.exchanger.presentation.mvi.reducer.handlers.CalculateAmountActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.ConnectionRestoredActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.LoadingFailureActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.LoadingSuccessActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.RetryLoadingActionHandler
import com.revolut.exchanger.presentation.mvi.reducer.handlers.SelectBaseCurrencyActionHandler
import com.revolut.exchanger.presentation.mvi.states.ExchangerState
import com.revolut.exchanger.presentation.view.CurrenciesResourcesResolver
import com.revolut.exchanger.presentation.viewmodel.ExchangerViewModel
import com.revolut.exchanger.utils.connection.ConnectionChecker
import com.revolut.exchanger.utils.connection.ConnectionStateObserver
import com.revolut.exchanger.utils.extensions.createViewModel
import com.revolut.exchanger.utils.mvi.Store
import com.revolut.exchanger.utils.resources.ResourcesManager
import com.revolut.exchanger.utils.rx.SchedulersFacade
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Provider

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
@Module
class ExchangerModule {

    @Provides
    @ExchangerScope
    fun provideExchangerViewModel(
        fragment: ExchangerFragment,
        store: Provider<Store<ExchangerAction, ExchangerState>>,
        connectionStateObserver: Provider<ConnectionStateObserver>
    ): ExchangerViewModel {
        return fragment.createViewModel {
            ExchangerViewModel(
                store = store.get(),
                connectionStateObserver = connectionStateObserver.get()
            )
        }
    }

    // region Repository

    @Provides
    @ExchangerScope
    fun provideRatesRepository(
        apiService: RatesInfoApiService,
        mapper: RatesInfoResponseMapper
    ): RatesRepository {
        return RatesRepositoryImpl(apiService, mapper)
    }

    @Provides
    @ExchangerScope
    fun provideRatesInfoApiService(retrofit: Retrofit): RatesInfoApiService {
        return retrofit.create(RatesInfoApiService::class.java)
    }

    @Provides
    @ExchangerScope
    fun provideRatesInfoResponseMapper(): RatesInfoResponseMapper {
        return RatesInfoResponseMapper()
    }

    // endregion

    // region MVI components

    @Provides
    @ExchangerScope
    fun provideExchangerStore(
        reducer: ExchangerReducer,
        schedulers: SchedulersFacade,
        updateRatesMiddleware: UpdateRatesMiddleware
    ): Store<ExchangerAction, ExchangerState> {
        return Store(
            reducer = reducer,
            schedulers = schedulers,
            middlewares = listOf(updateRatesMiddleware),
            initConfig = ExchangerDefaultsFactory.buildDefaultStoreConfig()
        )
    }

    @Provides
    @ExchangerScope
    fun provideExchangerReducer(
        retryLoadingActionHandler: RetryLoadingActionHandler,
        loadingSuccessActionHandler: LoadingSuccessActionHandler,
        loadingFailureActionHandler: LoadingFailureActionHandler,
        calculateAmountActionHandler: CalculateAmountActionHandler,
        selectBaseCurrencyActionHandler: SelectBaseCurrencyActionHandler,
        connectionRestoredActionHandler: ConnectionRestoredActionHandler
    ): ExchangerReducer {
        return ExchangerReducer(
            retryLoadingActionHandler,
            loadingSuccessActionHandler,
            loadingFailureActionHandler,
            calculateAmountActionHandler,
            selectBaseCurrencyActionHandler,
            connectionRestoredActionHandler
        )
    }

    @Provides
    @ExchangerScope
    fun provideLoadingSuccessActionHandler(
        cellsBuilder: ExchangerCellsBuilder
    ): LoadingSuccessActionHandler {
        return LoadingSuccessActionHandler(cellsBuilder)
    }

    @Provides
    @ExchangerScope
    fun provideLoadingFailureActionHandler(
        cellsBuilder: ExchangerCellsBuilder,
        connectionChecker: ConnectionChecker
    ): LoadingFailureActionHandler {
        return LoadingFailureActionHandler(cellsBuilder, connectionChecker)
    }

    @Provides
    @ExchangerScope
    fun provideSelectBaseCurrencyActionHandler(
        cellsBuilder: ExchangerCellsBuilder
    ): SelectBaseCurrencyActionHandler {
        return SelectBaseCurrencyActionHandler(cellsBuilder)
    }

    @Provides
    @ExchangerScope
    fun provideCalculateAmountActionHandler(
        cellsBuilder: ExchangerCellsBuilder
    ): CalculateAmountActionHandler {
        return CalculateAmountActionHandler(cellsBuilder)
    }

    @Provides
    @ExchangerScope
    fun provideRetryLoadingActionHandler(): RetryLoadingActionHandler {
        return RetryLoadingActionHandler()
    }

    @Provides
    @ExchangerScope
    fun provideConnectionRestoredActionHandler(): ConnectionRestoredActionHandler {
        return ConnectionRestoredActionHandler()
    }

    @Provides
    @ExchangerScope
    fun provideUpdateRatesMiddleware(
        schedulers: SchedulersFacade,
        ratesRepository: RatesRepository
    ): UpdateRatesMiddleware {
        return UpdateRatesMiddleware(
            schedulers = schedulers,
            ratesRepository = ratesRepository
        )
    }

    // endregion

    // region View utils

    @Provides
    @ExchangerScope
    fun provideExchangerCellsBuilder(
        resourcesResolver: CurrenciesResourcesResolver
    ): ExchangerCellsBuilder {
        return ExchangerCellsBuilder(resourcesResolver)
    }

    @Provides
    @ExchangerScope
    fun provideCurrenciesResourcesResolver(
        resourcesManager: ResourcesManager
    ): CurrenciesResourcesResolver {
        return CurrenciesResourcesResolver(resourcesManager)
    }

    // endregion

}