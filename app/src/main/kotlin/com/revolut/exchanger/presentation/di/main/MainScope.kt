package com.revolut.exchanger.presentation.di.main

import javax.inject.Scope

/**
 * Created by Ruslan Arslanov on 24/02/2020.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope