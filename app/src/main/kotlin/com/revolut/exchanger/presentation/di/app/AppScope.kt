package com.revolut.exchanger.presentation.di.app

import javax.inject.Scope

/**
 * Created by Ruslan Arslanov on 16/02/2020.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope