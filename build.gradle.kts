// Top-level build file where you can add configuration options common to all sub-projects/modules.

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
        gradlePluginPortal()
        maven("https://jitpack.io")
        maven("https://s3.amazonaws.com/repo.commonsware.com")
    }
}

buildscript {
    repositories {
        google()
        jcenter()
        mavenCentral()
        gradlePluginPortal()
        maven("https://maven.fabric.io/public")
    }
    dependencies {
        classpath(GradlePlugins.GRADLE)
        classpath(GradlePlugins.KOTLIN)
    }

}
subprojects {
    enableInlineClasses()
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

fun Project.enableInlineClasses() {
    tasks.withType<KotlinCompile>().configureEach {
        kotlinOptions {
            val args = kotlinOptions.freeCompilerArgs
            kotlinOptions.freeCompilerArgs = args + listOf("-XXLanguage:+InlineClasses")
        }
    }
}

