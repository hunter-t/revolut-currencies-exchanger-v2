import buildconfig.BaseLibraryModuleBuildConfiguration

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-android-extensions")
}

android {
    BaseLibraryModuleBuildConfiguration(project, androidExtension = this).configure()
}

androidExtensions {
    isExperimental = true
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Kotlin.
    implementation(Kotlin.STDLIB)

    // Lifecycle.
    implementation(LifeCycle.VIEW_MODEL_LIBRARY)
    implementation(LifeCycle.LIFECYCLE_EXTENSIONS)

    // AndroidX.
    implementation(MaterialComponents.LIBRARY)

    // Rx.
    implementation(Rx.RX_JAVA)
    implementation(Rx.RX_KOTLIN)
    implementation(Rx.RX_ANDROID)
    implementation(Rx.RX_RELAY)

    // Dagger2.
    implementation(Dagger2.LIBRARY)
    implementation(Dagger2.ANDROID_EXTENSIONS)
    implementation(Dagger2.SUPPORT_EXTENSIONS)
    kapt(Dagger2.COMPILER)
    kapt(Dagger2.ANDROID_EXTENSIONS_COMPILER)
    implementation(Dagger2.ANNOTATIONS)

    // Tools.
    implementation(Tools.TIMBER)
    implementation(Tools.ADAPTER_DELEGATES)
}