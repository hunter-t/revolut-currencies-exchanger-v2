package com.revolut.exchanger.utils.extensions

import io.reactivex.disposables.Disposable

/**
 * Created by Ruslan Arslanov on 19.02.2020.
 */

fun Disposable?.disposeSafely() {
    if (this == null) {
        return
    }
    if (isDisposed.not()) {
        dispose()
    }
}