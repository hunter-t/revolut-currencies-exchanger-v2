package com.revolut.exchanger.utils.extensions

import java.util.Locale

/**
 * Created by Ruslan Arslanov on 24/02/2020.
 */

const val EMPTY_STRING = ""

/**
 * Back-ported from [kotlin.text.capitalize] to avoid setting experimental annotations.
 */
fun String.capitalize(): String {
    return if (isNotEmpty() && this[0].isLowerCase()) {
        substring(0, 1).toUpperCase(Locale.getDefault()) + substring(1)
    } else {
        this
    }
}