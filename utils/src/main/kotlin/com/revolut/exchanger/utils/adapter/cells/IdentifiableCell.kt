package com.revolut.exchanger.utils.adapter.cells

/**
 * Created by Ruslan Arslanov on 20/02/2020.
 */
interface IdentifiableCell<T> : BaseCell {
    val cellId: T
}