package com.revolut.exchanger.utils.connection

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import com.revolut.exchanger.utils.extensions.connectivityManager
import io.reactivex.Observable

/**
 * Created by Ruslan Arslanov on 26/02/2020.
 */
class ConnectionStateObserverApi21Impl(
    private val context: Context
) : ConnectionStateObserver {

    /**
     * [ConnectivityManager.getActiveNetworkInfo] and [ConnectivityManager.CONNECTIVITY_ACTION]
     * are deprecated for API >= 29 (Android 10), thus this [ConnectionStateObserver]
     * implementation must be used for API below Android 10.
     * For API >= 29 use [ConnectionStateObserverApi23Impl].
     */
    @Suppress("DEPRECATION")
    override fun observe(): Observable<Boolean> {
        return Observable.create<Boolean> { emitter ->
            context.registerReceiver(
                object : BroadcastReceiver() {
                    override fun onReceive(context: Context, intent: Intent) {
                        val isAvailable = context
                            .connectivityManager
                            .activeNetworkInfo
                            ?.isConnected == true
                        emitter.onNext(isAvailable)
                    }
                },
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            )
        }
    }


}