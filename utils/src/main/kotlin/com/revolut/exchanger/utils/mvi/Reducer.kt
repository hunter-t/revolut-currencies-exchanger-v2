package com.revolut.exchanger.utils.mvi

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
interface Reducer<A, S> {
    fun reduce(state: S, action: A): S
}