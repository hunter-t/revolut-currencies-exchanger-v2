package com.revolut.exchanger.utils.mvi

import androidx.lifecycle.MutableLiveData
import com.jakewharton.rxrelay2.PublishRelay
import com.revolut.exchanger.utils.extensions.updateValue
import com.revolut.exchanger.utils.rx.SchedulersFacade
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
class Store<A, S>(
    private val reducer: Reducer<A, S>,
    private val schedulers: SchedulersFacade,
    private val initConfig: InitialConfig<A, S>,
    private val middlewares: List<Middleware<A>>
) {

    private val actionsRelay = PublishRelay.create<A>()

    private val actions = if (initConfig.initAction != null) {
        actionsRelay.startWith(initConfig.initAction)
    } else {
        actionsRelay
    }

    fun postAction(action: A) {
        actionsRelay.accept(action)
    }

    fun wire(state: MutableLiveData<S>): Disposable {
        return CompositeDisposable().apply {
            addAll(
                wireStateUpdates(state),
                bindMiddlewares()
            )
        }
    }

    private fun wireStateUpdates(state: MutableLiveData<S>): Disposable {
        return actions
            .map { action ->
                val latestState = state.value ?: initConfig.initState
                reducer.reduce(latestState, action)
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribe(state::updateValue)
    }

    private fun bindMiddlewares(): Disposable {
        return Observable
            .merge<A>(
                middlewares.map { middleware ->
                    middleware.bind(actions)
                }
            )
            .subscribe(::postAction)
    }


    class InitialConfig<A, S>(
        val initState: S,
        val initAction: A? = null
    )

}