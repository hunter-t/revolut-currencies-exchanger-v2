package com.revolut.exchanger.utils.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
abstract class BaseFragment : Fragment() {

    protected open val hasComponent: Boolean = true

    @get:LayoutRes protected abstract val layoutResId: Int

    private var snackBar: Snackbar? = null

    @CallSuper
    override fun onAttach(context: Context) {
        if (hasComponent) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(context)
    }

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutResId, container, false)
    }

    protected inline fun <T> LiveData<T>.observe(crossinline observer: (T) -> Unit) {
        observe(viewLifecycleOwner, Observer { data ->
            if (data != null) observer(data)
        })
    }

    protected fun showIndefiniteSnack(
        @StringRes messageResId: Int,
        @StringRes actionResId: Int? = null,
        action: (() -> Unit)? = null
    ) {
        if (snackBar?.view?.tag == messageResId) {
            // Snack with this message is already shown.
            return
        }
        snackBar = Snackbar
            .make(requireView(), messageResId, Snackbar.LENGTH_INDEFINITE)
            .also { snack ->
                snack.view.tag = messageResId
                snack.show()
            }
        if (actionResId != null && action != null) {
            snackBar?.setAction(actionResId) {
                action()
            }
        }
    }

    protected fun dismissSnack() {
        snackBar?.dismiss()
        snackBar = null
    }

}