package com.revolut.exchanger.utils.extensions

import android.view.inputmethod.InputMethodManager
import android.widget.EditText

/**
 * Created by Ruslan Arslanov on 26/02/2020.
 */

fun EditText.doWithIgnoringChanges(block: EditText.() -> Unit) {
    isIgnoringChanges = true
    this.block()
    isIgnoringChanges = false
}

fun EditText.showKeyboard() {
    requestFocus()
    this.context.keyboard.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun EditText.doOnFocusReceived(block: () -> Unit) {
    setOnFocusChangeListener { _, hasFocus ->
        if (hasFocus) {
            block()
        }
    }
}

fun EditText.moveCursorToEnd() {
    setSelection(text.length)
}