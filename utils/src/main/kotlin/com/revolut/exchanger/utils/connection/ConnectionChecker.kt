package com.revolut.exchanger.utils.connection

/**
 * Created by Ruslan Arslanov on 27/02/2020.
 */
interface ConnectionChecker {
    fun isConnected(): Boolean
}