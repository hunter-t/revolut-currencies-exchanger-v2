package com.revolut.exchanger.utils.mvi

/**
 * Created by Ruslan Arslanov on 25/02/2020.
 */
interface ActionHandler<A, S> {
    fun handle(state: S, action: A): S
}