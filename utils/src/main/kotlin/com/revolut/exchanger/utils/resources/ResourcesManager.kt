package com.revolut.exchanger.utils.resources

import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

/**
 * Created by Ruslan Arslanov on 24/02/2020.
 */
interface ResourcesManager {

    fun getString(@StringRes stringResId: Int): String

    fun getDrawable(@DrawableRes drawableResId: Int): Drawable

    fun getIdentifier(name: String, type: String): Int

}