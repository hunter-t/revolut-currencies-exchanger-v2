package com.revolut.exchanger.utils.extensions

import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Ruslan Arslanov on 25/02/2020.
 */

fun <VH : RecyclerView.ViewHolder> RecyclerView.Adapter<VH>.doOnItemRangeMoved(block: () -> Unit) {
    registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
            block()
        }
    })
}

fun <VH : RecyclerView.ViewHolder> RecyclerView.Adapter<VH>.doOnItemCountChanged(
    block: (itemsCount: Int) -> Unit
) {
    registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            block(getItemCount())
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            block(getItemCount())
        }
    })
}

fun RecyclerView.doOnScroll(block: (dx: Int, dy: Int) -> Unit) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            block(dx, dy)
        }
    })
}

fun RecyclerView.doOnTouchAndMove(block: () -> Unit) {
    setOnTouchListener { _, event ->
        if (event.action == MotionEvent.ACTION_MOVE) {
            block()
        }
        false
    }
}