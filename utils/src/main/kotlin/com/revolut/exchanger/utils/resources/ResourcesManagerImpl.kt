package com.revolut.exchanger.utils.resources

import android.content.Context
import android.graphics.drawable.Drawable

/**
 * Created by Ruslan Arslanov on 24/02/2020.
 */
class ResourcesManagerImpl(
    private val context: Context
) : ResourcesManager {

    override fun getString(stringResId: Int): String {
        return context.getString(stringResId)
    }

    override fun getDrawable(drawableResId: Int): Drawable {
        return context.getDrawable(drawableResId) as Drawable
    }

    override fun getIdentifier(name: String, type: String): Int {
        return context.resources.getIdentifier(name, type, context.packageName)
    }

}