package com.revolut.exchanger.utils.extensions

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.revolut.exchanger.utils.viewmodel.BaseViewModelFactory

/**
 * Created by Ruslan Arslanov on 24/02/2020.
 */

/**
 * Created by Ruslan Arslanov on 12.08.2018.
 */
inline fun <reified VM : ViewModel> Fragment.createViewModel(
    crossinline provider: () -> VM
): VM {
    return ViewModelProviders
        .of(this, BaseViewModelFactory { provider() })
        .get(VM::class.java)
}