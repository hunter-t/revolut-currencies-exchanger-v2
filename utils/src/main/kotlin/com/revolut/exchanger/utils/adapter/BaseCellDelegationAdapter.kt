package com.revolut.exchanger.utils.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.revolut.exchanger.utils.adapter.cells.BaseCell

/**
 * Created by Ruslan Arslanov on 29.09.2018.
 */
open class BaseCellDelegationAdapter(
    diffCallback: DiffUtil.ItemCallback<BaseCell>
) : AsyncListDifferDelegationAdapter<BaseCell>(diffCallback) {

    fun getItem(position: Int): BaseCell? = items.getOrNull(position)

}