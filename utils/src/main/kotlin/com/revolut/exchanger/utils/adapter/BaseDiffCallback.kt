package com.revolut.exchanger.utils.adapter

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import com.revolut.exchanger.utils.adapter.cells.BaseCell
import com.revolut.exchanger.utils.adapter.cells.IdentifiableCell

/**
 * Created by Ruslan Arslanov on 31.01.2019.
 */
open class BaseDiffCallback : DiffUtil.ItemCallback<BaseCell>() {

    override fun areItemsTheSame(oldItem: BaseCell, newItem: BaseCell): Boolean {
        return when {
            oldItem.javaClass != newItem.javaClass -> false
            oldItem is IdentifiableCell<*> && newItem is IdentifiableCell<*> -> {
                oldItem.cellId == newItem.cellId
            }
            else -> oldItem.hashCode() == newItem.hashCode()
        }
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: BaseCell, newItem: BaseCell): Boolean {
        return oldItem == newItem
    }

}