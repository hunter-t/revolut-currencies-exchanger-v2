package com.revolut.exchanger.utils.adapter.delegates

import android.view.ViewGroup
import com.revolut.exchanger.utils.adapter.cells.BaseCell
import com.revolut.exchanger.utils.adapter.viewholders.BaseCellViewHolder
import com.revolut.exchanger.utils.extensions.inflate
import ru.skandi.lk.common.view.adapter.delegates.AbsCellDelegate

/**
 * Created by Ruslan Arslanov on 20.02.2020.
 */
abstract class BaseCellDelegate<C : BaseCell>(

    cellLayout: Int

) : AbsCellDelegate<C, BaseCellViewHolder>(cellLayout) {

    override fun onCreateViewHolder(parent: ViewGroup): BaseCellViewHolder {
        return BaseCellViewHolder(parent.inflate(cellLayout)).also(::onViewHolderCreated)
    }

    open fun onViewHolderCreated(viewHolder: BaseCellViewHolder) = Unit

}