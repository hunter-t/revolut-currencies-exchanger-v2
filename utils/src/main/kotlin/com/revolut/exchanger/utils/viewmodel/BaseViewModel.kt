package com.revolut.exchanger.utils.viewmodel

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.revolut.exchanger.utils.extensions.disposeSafely
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
abstract class BaseViewModel : ViewModel() {

    private val onClearedDisposable = CompositeDisposable()

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        onClearedDisposable.disposeSafely()
    }

    protected fun Disposable.disposeOnCleared(): Disposable {
        onClearedDisposable.add(this)
        return this
    }

}