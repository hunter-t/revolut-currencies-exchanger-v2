package com.revolut.exchanger.utils.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

/**
 * Created by Ruslan Arslanov on 20.02.2020.
 */

private const val IGNORE_CHANGES_TAG = "ignore_changes"


// region Visibility

fun View.makeVisibleOrGone(visible: Boolean) {
    if (visible) {
        makeVisible()
    } else {
        makeGone()
    }
}

fun View.makeGone() {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
}

fun View.makeVisible() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

// endregion

fun ViewGroup.inflate(@LayoutRes layoutResId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutResId, this, attachToRoot)
}

fun View.onClick(action: () -> Unit) {
    setOnClickListener {
        action.invoke()
    }
}

fun View.adjustHeightToFillParent() {
    val parentViewGroup = parent as? ViewGroup ?: return
    val parentPadding = parentViewGroup.paddingTop + parentViewGroup.paddingBottom
    val height = parentViewGroup.height - top - parentPadding
    val adjustedHeight = minimumHeight.coerceAtLeast(height)
    layoutParams.height = adjustedHeight
    requestLayout()
}

var View.isIgnoringChanges: Boolean
    get() = tag == IGNORE_CHANGES_TAG
    set(value) {
        if (value && tag != IGNORE_CHANGES_TAG) {
            tag = IGNORE_CHANGES_TAG
        }
        if (value.not() && tag == IGNORE_CHANGES_TAG) {
            tag = null
        }
    }

fun View.hideKeyboard() {
    context.keyboard.hideSoftInputFromWindow(windowToken, 0)
}