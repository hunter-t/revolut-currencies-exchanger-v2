package com.revolut.exchanger.utils.connection

import io.reactivex.Observable

/**
 * Created by Ruslan Arslanov on 26/02/2020.
 */
interface ConnectionStateObserver {

    /**
     * Subscribe to receive Internet connection changes:
     * true is emitted when connected, false – when disconnected.
     */
    fun observe(): Observable<Boolean>

}