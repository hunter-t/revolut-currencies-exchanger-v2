package com.revolut.exchanger.utils.extensions

import android.os.Looper
import androidx.lifecycle.MutableLiveData

/**
 * Created by Ruslan Arslanov on 24/02/2020.
 */

val <T> MutableLiveData<T>.asLiveData
    get() = this

fun <T> MutableLiveData<T>.updateValue(value: T) {
    if (Looper.getMainLooper().thread == Thread.currentThread()) {
        setValue(value)
    } else {
        postValue(value)
    }
}