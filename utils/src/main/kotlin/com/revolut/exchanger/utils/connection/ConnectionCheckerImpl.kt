package com.revolut.exchanger.utils.connection

import android.content.Context
import android.net.ConnectivityManager
import com.revolut.exchanger.utils.extensions.connectivityManager

/**
 * Created by Ruslan Arslanov on 27/02/2020.
 */
class ConnectionCheckerImpl(
    private val context: Context
) : ConnectionChecker {

    /**
     * Even though [ConnectivityManager.getActiveNetworkInfo] is deprecated for API >= 29
     * (Android 10), this is the most trusted and concise way to determine existing Network
     * connection.
     */
    @Suppress("DEPRECATION")
    override fun isConnected(): Boolean {
        return context.connectivityManager.activeNetworkInfo?.isConnected == true
    }

}