package com.revolut.exchanger.utils.mvi

import io.reactivex.Observable

/**
 * Created by Ruslan Arslanov on 19/02/2020.
 */
interface Middleware<A> {
    fun bind(actions: Observable<A>): Observable<A>
}