package com.revolut.exchanger.utils.mvi

import io.reactivex.Observable

/**
 * Created by Ruslan Arslanov on 20/02/2020.
 */
interface MviView<A, S> {
    val actions: Observable<A>
    fun render(state: S)
}