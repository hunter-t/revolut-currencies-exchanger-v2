package com.revolut.exchanger.utils.connection

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import androidx.annotation.RequiresApi
import com.revolut.exchanger.utils.extensions.connectivityManager
import io.reactivex.Observable

/**
 * Created by Ruslan Arslanov on 26/02/2020.
 */
@RequiresApi(Build.VERSION_CODES.N)
class ConnectionStateObserverApi23Impl(
    private val context: Context
) : ConnectionStateObserver {

    override fun observe(): Observable<Boolean> {
        return Observable.create<Boolean> { emitter ->
            context
                .connectivityManager
                .registerDefaultNetworkCallback(
                    object : ConnectivityManager.NetworkCallback() {
                        override fun onLost(network: Network) {
                            emitter.onNext(false)
                        }

                        override fun onAvailable(network: Network) {
                            emitter.onNext(true)
                        }
                    }
                )
        }
    }

}