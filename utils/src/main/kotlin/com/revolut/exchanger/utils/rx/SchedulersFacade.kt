package com.revolut.exchanger.utils.rx

import io.reactivex.Scheduler

/**
 * Created by Ruslan Arslanov on 06/09/2018.
 */
interface SchedulersFacade {

    fun io(): Scheduler

    fun ui(): Scheduler

}