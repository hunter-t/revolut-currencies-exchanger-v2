package com.revolut.exchanger.utils.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.view.inputmethod.InputMethodManager

/**
 * Created by Ruslan Arslanov on 23/02/2020.
 */

val Context.displayHeight: Int
    get() = resources.displayMetrics.heightPixels

fun Context.dipToPix(dpValue: Int): Int = (dpValue * resources.displayMetrics.density).toInt()

val Context.keyboard: InputMethodManager
    get() = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

val Context.connectivityManager: ConnectivityManager
    get() = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager