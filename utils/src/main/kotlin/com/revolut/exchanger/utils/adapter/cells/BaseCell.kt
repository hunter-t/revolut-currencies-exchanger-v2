package com.revolut.exchanger.utils.adapter.cells

import androidx.annotation.LayoutRes

/**
 * Created by Ruslan Arslanov on 20/02/2020.
 */
interface BaseCell {
    /**
     * Represents the ViewType and the [LayoutRes] of the cell.
     */
    @get:LayoutRes
    val cellLayout: Int
}