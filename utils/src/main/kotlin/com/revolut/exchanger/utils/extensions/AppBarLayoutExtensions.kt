package com.revolut.exchanger.utils.extensions

import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout

/**
 * Created by Ruslan Arslanov on 27/02/2020.
 */

fun Toolbar.switchScrollability(isScrollable: Boolean) {
    val lp = (layoutParams as? AppBarLayout.LayoutParams) ?: return
    lp.scrollFlags = if (isScrollable) {
        AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or
                AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or
                AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
    } else {
        AppBarLayout.LayoutParams.SCROLL_FLAG_NO_SCROLL
    }
    layoutParams = lp
}